# Carroted Probation

In the event that a Carroted member violates the [Code of Conduct](code-of-conduct), other members can vote to put the member on Carroted Probation as a punishment.

Carroted Probation, usually accompanied with a temporary ban from Carroted, is a set of restrictions that are given to members for a set period of time based on the severity of the violation. These restrictions include:

1. Limited privileges (such as not being able to commit to the `main`/`master` branch)
2. Being excluded from some [weekly Carroted meetings](about-meetings)
3. Lack of access to [Emergency Powers](emergency-powers)
4. Not being told certain information
Any other restrictions that are deemed necessary.

Other members can vote on extending or decreasing the length of time the member is on probation based on the member's behaviour as well as the existence of previous violations.

After Carroted Probation, a member can be restored to their original rank or to a lower rank, depending on a vote.
