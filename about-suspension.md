# Carroted Suspension

When we speak of active Carroted members, we are referring to members of Carroted which are not currently suspended.

Carroted Suspension temporarily revokes a member's access to our Codeberg organization, our Tumu/Revolt server, [Carroted MP](carroted-mp), etc., without affecting their rank. This is used when we believe a member's account(s) are compromised, and is used to maintain Carroted security.

To suspend a member, a vote with all available members must be held. This meeting can be an [Emergency Meeting](about-meetings#emergency-meetings) if needed.