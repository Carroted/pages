# Carroted History

Carroted was founded on the 3rd of August, 2021 (at 7:21 PM AST) by **asour** to be the game studio for the creation of the game DURE, which she was creating. Carroted was, at the time, a subsidiary of Deultra.

A game named Pylon Pixel, which was started prior to Carroted's founding, but subsequently transferred to Carroted, ended up being the first game released by Carroted in December 2021.

On the 14th of August, 2022, **asour** made Carroted an independent nonprofit organisation creating free and open-source software, games, animation, music, websites and more. She also added a flat structure, giving away ownership of Carroted and making everyone equal.

On the 3rd of March, 2023, **asour** was voted to be reinstated as owner of Carroted.