# Carroted Rank System

At Carroted, we have developed a multi-level rank system to track the progress of our members and secure our projects. We currently have five different ranks, each with its distinct set of access and benefits.

The Carroted Rank system is heavily integrated with the [Project Security Levels](security-levels) system. It aims to provide a clear indication of one's status within the organization and helps us keep an organized record of member progression. Additionally, the multi-level rank system provides a means to control access to our private projects and ensures that only trusted, capable members of our team may contribute to them. Finally, the rank system adds a level of security and helps us deter potential conflicts and issues by keeping a visible history of banned and ex-members.

The rank system plays a vital role in maintaining the security and integrity of our Carroted team and projects.

-----

## Rank -1

Rank -1 is the rank of banned members. The only way to get unbanned is by a Carroted vote.

Rank -1 is kept so we can easily see who is banned from joining. It's used to clearly identify banned members and to auto-deny applications from them via [Overseer](overseer).

## Rank 0

Kicked out members and other ex-members are given Rank 0.

Rank 0 is kept to have ex-members in archives and to preserve incident info and other details about them, though they can have personal info removed if they wish.

## Rank 1

Rank 1 is given basic [Carroted MP](carroted-mp) access, access to Carroted server invite, and is invited to the Carroted organisation on Codeberg and GitHub.

Rank 1 cannot push to `main` branches on repositories or see private projects/branches yet, nor private project channels/categories in the server or Portal.

Rank 1 can only see already public projects to begin helping out and rank up.

That means they only have access to [🟢 C projects](security-levels#c-projects) while on Rank 1. They still can't push to `main` on them, and should mainly be making Pull Requests and helping work on beta/alpha branches.

## Rank 2

Rank 2 has the same as Rank 1, but Rank 2 users also see [🔴 A projects](security-levels#a-projects) and [🟡 B projects](security-levels#b-projects). They can push to `main` on both of those security levels.

However, they cannot push to `main` on [🟢 C projects](security-levels#c-projects), as anything pushed to that would immediately be public, which is a risk while they're low level.

Rank 2 is obtained after 1 month of Rank 1 if one didn't violate Code of Conduct and no complaints were made. If complaints were made, their fate is to be decided by a Carroted Vote in a [Weekly Meeting](about-meetings#weekly-meetings).

Once eligible for this rank, you need to sign a written agreement not to share confidential details outside of Carroted to obtain it. If you don't sign the agreement, you can stay at Rank 1 with less benefits and access.

## Rank 3

Rank 3 has access to push to `main` on [🟢 C projects](security-levels#c-projects), however you are recommended to only do this after testing, and pushing to an unstable branch is preferable.

-----

## Table

Here's a handy table to help you understand the rank system:

| Rank | C Project Access | B Project Access | A Project Access | Portal Access | Notes |
|:----:|:----------------:|:----------------:|:----------------:|:-------------:|:------|
| `-1` | ❌ | ❌ | ❌ | ❌ | Banned, no server access and can't apply |
| `0` | ❌ | ❌ | ❌ | ❌ | Ex-Member, no server access |
| `1` | ✅ | ❌ | ❌ | ✅ | Can't push to `main` at all |
| `2` | ✅ | ✅ | ✅ | ✅ | Can't push to `main` on C projects but can others |
| `3` | ✅ | ✅ | ✅ | ✅ | Can push to `main` on C projects |

Members can instantly lower their rank at any time if not contractually obliged to stay at a higher rank.