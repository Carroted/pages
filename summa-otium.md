# Summa-otium

In the event of extreme lack of activity, Carroted Summa-otium rules allow members to create a temporary smaller group to vote on things.

"Extreme lack of activity" exclusively refers to a case in which it is, after at least one week of direct attempts, impossible to get a vote from the majority of members on a decision.

Carroted can then enter a "Summa-otium" state, in which a group of all available members is formed which will take part in decisions for Carroted.

The Summa-otium group does not have authority over other Carroted members. Their say can be overriden by a vote from everyone in Carroted.

At any time, the Summa-otium state can be revoked by a majority vote from the total Carroted members.