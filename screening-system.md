# Carroted Entry Screening System

The Carroted Entry Screening System is an organised method for screening potential members to enter Carroted. 

### Abbreviations

CM - Carroted Member (CMs plural)
PM - Potential Member

-----

## Phase 0

Carroted is always open to applications, and anyone who meets the following criteria can apply to join Carroted:

1. Must not have been previously banned from Carroted
2. Must be at least 15 years old
3. Must not be prohibited by law in their jurisdiction from joining Carroted
4. Must have read and understood the entirety of the Carroted Pages before applying

They can then submit an application, which must include:

- A digital point of contact (e.g. e-mail, Matrix, Discord, Revolt) 
- A portfolio
- A brief statement on why they wish to join

If Carroted is interested in moving forward with the member, a CM of rank 3+ can then reach out to the PM at the provided point of contact. If communication is established, the application may move to Phase 1, where an interview will take place.

## Phase 1 

This phase begins with the PM being added to a Carroted Entry/Exit Matrix room alongside with a maximum of **3** other CMs.

The PM must agree to not share media captures of the conversation outside Carroted. Carroted members must agree to not share media 
captures of the conversation outside of Carroted too, for security and privacy reasons. However, the conversation will be recorded for exclusive use within Carroted for 
members to formulate a decision.

If this has been completed, move on to Phase 2.

## Phase 2

A set of questions are asked by up to **three** CMs, in order:

1. What's your name?
2. What are your pronouns?
3. Do you understand what we do at Carroted?
4. What can you bring to Carroted?
5. Are you currently/were part of any other non-profit organizations? If so, please detail them.
6. How well can you cope in a collaborative environment?
7. What is your stance on free (as in freedom and price), open-source software (FOSS)?
8. Have you contributed to any open-source projects before?

Further questions not listed may be asked. 

## Phase 3

After the completion of Phase 1 & 2 (unless the member's entry has been [expedited](#expedited-entry) and Phase 2 is skipped), all active Carroted members of rank 2+ with the exception of those on Carroted Probation, can vote on whether the PM should be granted a trial period into Carroted.

In the event that member votes come to a tie, the highest ranked available Carroted member can break the tie in their favour. The members can also hold a re-screening for the PM to ask further questions of their choosing or clarification.

If the votes are in favor of the PM, the screening will move to Phase 4.

## Phase 4

If the PM is granted entry, a 1 month trial period will begin where the PM becomes a rank 1 member. Any 
violation of the Carroted Code of Conduct will result in an instant permaban. This is done to ensure that the member is suitable to 
remain within Carroted and can be trusted with sensitive information.

The PM will **not** be warned when their trial phase is about to end so that the PM can use their own initiative.

In the closest weekly meeting to 1 month after the beginning of the trial period, CMs of rank 2+ will vote on whether or not to keep the member in Carroted (at rank 1) or let them go. This will be based on their contributions and activity, among other factors.

The PM will be informed if let go or once their trial period has ended.

## Expedited Entry

The PM can have their screening process accelerated and skip Phase 4 and become a full member if they fill this category:

- In good relations with more than 3 current Carroted members prior to entering Carroted
- Contributed significantly towards a Carroted project.
- Responsibly disclosed vulnerabilities within Carroted projects.
- Excellent profile on GitHub, Codeberg, etc or a portfolio of some sorts