#  Meetings

Carroted has meetings to discuss a range of things such as:

- Project progress
- Things to be to be done
- Project proposals
- Complaints and issues

Meetings are helpful to boost motivation and are extremely useful for discussing information, clearing up issues and other problems that may arise.

## Weekly meetings

Weekly meetings prioritize bringing up items mentioned in the private Carroted Meeting Topics Document, however, other things can be discussed that aren't mentioned in the document (such as the things listed above).

They are hosted during **Sundays at 1 PM AST**, but can be rescheduled to fit other members' schedules. We highly encourage Carroted members to join in as important information will likely be relayed, and their feedback to others is valued. Missing out on meetings will likely cause them to fall behind on updates, discussions and other announcements.

## Calling Meetings

Meetings can be called at any time, independently of the weekly meetings. Multiple meetings can occur at once in different threads of the `#Meetings` channel.

Any Carroted member of rank 2+ can call a meeting. If it is a non-emergency meeting, they should give a minimum **6 hours** notice in advance. 

### Emergency Meetings

In the case that a meeting must be held urgently, a member can hold an emergency meeting with all available members without prior notice.

## Final Meeting

The final Carroted meeting will be held on August 3, 2071 at 1 PM AST (on the 50th anniversary of the organisation) to discuss the successes of the primary mission, the impact of the A Projects and the future of the world. Everyone is invited except people named Gregory. Fuck Gregory.

Following this meeting, Carroted will be disbanded, and all records will be erased. We will rename all projects to "Barroted" and change all image files (`.png`, `.gif`, `.apng`, `.webp`, `.jpg`, etc.) to the Barroted logo. Thanks for watching