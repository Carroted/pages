# Carroted MP

Carroted's instance of MP, our Member Portal software, is a webapp used in Carroted for things such as:

- Publishing to the Carroted Blog
- Looking up members
- Reporting activity
- Seeing and editing the Meeting Topics
- Various other Carroted activities

Carroted MP is heavily integrated with the Carroted server on Tumu/Revolt, and is used daily by most members.