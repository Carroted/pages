# Welcome

Welcome! These are the Carroted Pages for members to read. The Carroted Pages are charter for Carroted members to follow, which we have decided to make public for transparency. In the pages, you'll find information about how Carroted operates, what rules members must follow, and other important information.

If you are a new member of Carroted, or are interested in joining Carroted, you should read all pages carefully. Carroted is not responsible for incidents or deaths caused by improper comprehension of Carroted Pages. 

Changes to the pages are enforced the following Tuesday they are committed to ensure plenty of time is allowed to make further edits and gather votes.