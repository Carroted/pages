# Code of Conduct

This is the Code of Conduct (the rules) that Carroted members agree to abide by to the best of their abilities.

----------

### 1. Respect & Courteousy

Carroted members must respect each other, as well as others. 

Here is a detailed list of what is not allowed:

1. Racism
2. Homophobia
3. Transphobia
4. Offensive language
5. Hate speech
And other discriminatory/hateful language.

Swearing is allowed, but members should try to keep it out of official Carroted announcements, like articles on the [Carroted Blog](https://carroted.org/blog).

### 2. Code Guidelines

Carroted members and contributors should try to follow the [Code Guidelines](code-guidelines) as much as possible.

This ensures that the code is clean, well-written, easy to read and consistent.

Note that changes in the Code Guidelines may be made at any time, but existing code does not have to be modified to fit the new Code Guidelines.

### 3. Leaks & "Doxxing"

Carroted members cannot leak (share/publish without prior proper permission) any personal information, such as names, email addresses, physical addresses or other personal information (this is known as "doxxing").

Carroted members also cannot leak unpublished projects/code/assets before they are ready to be published (members agree to publish it).

In the event that not everyone agrees, a vote will be held. More info can be found in [5. Disagreements](#5-disagreements).

### 4. Disagreements

Most significant decisions for Carroted will be discussed by all (or most) members. If not everyone agrees on a decision, a vote will be held.

Unfortunately, it is possible that a tie occurs. If this happens, the following solutions may be used:

- Bringing in a non-Carroted member to give one final vote
- Flipping a coin
- Other ideas that the majority of members may agree on

### 5. Use Common Sense

Carroted members should not do things they don't think the others would agree with without telling them first. This means they cannot delete Carroted assets or post misinformation about Carroted, among other things.

Members should always check with the others before doing things they think other members would disagree with, or significant decisions (more info above).

### 6. Illegal & Unethical Behavior

This also falls under [6. Use Common Sense](#6-use-common-sense) (and partially [4. Leaks & "Doxxing"](#4-leaks--doxxing)), but it is important enough to deserve its own rule.

Carroted members cannot do illegal things, such as adding pirated content to a Carroted server or violating a license. They cannot do unethical things, such as directly profiting off of someone's work without crediting them, even if the license allows for it.

### 7. Inactivity

Inactivity is considered when a member does not contribute to projects or participate in activities such as meetings for a period of time greater than a week without prior warning.

Members should do their very best to be active within meetings and help with projects as it contributes to ensuring a steady workflow within Carroted.

#### 7.1. Inactivity Consequences

If a member breaks the inactivity rule without any prior violations, it will result in a warning. However, if prior breaches exist, other members can vote to punish the offending member.

If Carroted is aware of a member's inactivity due to personal circumstances such as a health condition through a reliable source, the member can be removed from Carroted for security purposes. When the member returns, the member can receive expedited entry into Carroted.

### 8. Bogus Meetings, Proposals & Reports

To prevent wasting time and resources, matters such as meetings and reports called by members must have a clear and specific purpose related to the objectives and activities of the non-profit organization. Any meetings or reports that are deemed to be frivolous, baseless, or irrelevant may be disregarded or dismissed by other members.

---------

## Breaking rules

When a member is caught breaking rules, the other members will discuss what punishment they should be given (with a vote), if any, in a meeting. This meeting can happen immediately, and is separate from the [weekly Carroted meetings](about-meetings).

Punishments include:

- Temporary ban
- Permanent ban
- [Carroted Probation](about-probation)
- Other punishments voted on by members

[Carroted Suspension](about-suspension) is not regarded as a punishment, but may be used for security reasons if a member's account(s) have been compromised.

----------

These rules are subject to change at any time.
