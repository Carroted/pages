# Project Security Levels

At Carroted, we maintain different security levels for our projects based on their nature and confidentiality. These security levels help us to control access to our projects and ensure that our intellectual property is protected.

We use a color-coded system to denote the security levels of our projects:

## C Projects

🟢 **C Projects** are public projects that have been released to the public. They can be used by anyone under the terms of their license.

## B Projects

🟡 **B Projects**: are announced projects that are not yet open-source or released to the public. The source code for these projects is not available to the public, but they may be shared with select individuals or groups if voted on by Carroted members.

## A Projects

🔴 **A Projects** are private and closed projects that require the highest level of security. The source code for these projects is not shared with anyone outside of Carroted, and access is restricted through a series of security protocols.

-----

At Carroted, we take project security very seriously, and all team members are required to abide by these security levels. Discussion of an A project or of confidential B project info in public channels (even in the Carroted server) can lead to disciplinary action.