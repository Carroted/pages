# Emergency Powers

In certain situations, it may happen that Carroted members begin to severely violate the [Code of Conduct](code-of-conduct) and start causing permanent damage to Carroted/Carroted projects (such as deleting assets, banning members, deleting crucial members, etc.).

In these cases, needing to call a meeting and wait for a decision is not an option. Immediate action is necessary.

For this reason, a Carroted member may, under certain circumstances, use emergency powers. These powers grant the member the ability to immediately take action (such as removing the offender's permissions), without waiting for a meeting.

Emergency powers are only available to active (non-suspended) Carroted members who:

1. Have been in Carroted for at least 6 months
2. Have not violated the [Code of Conduct](code-of-conduct) for at least 6 months
3. Are not on [Carroted Probation](about-probation)

As soon as possible after the use of emergency powers, the member must organise a meeting to discuss what happened, why the powers were used and what action was taken.

The other members can then discuss if the use of emergency powers was appropriate, and if not, what punishment should be given. They can revoke the action if they disagree or take more action if they deem it necessary.