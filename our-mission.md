# Our Mission

At Carroted, we strongly believe that users should be able to freely modify, study, share, take apart, run or otherwise use their software.

**We are living in world where platforms are increasingly collecting and analyzing data on everything their users do, often without them even knowing.** From simple things such as your name and location to now your entire media habits, your routines and even your interests are now being sold for profit.

Carroted's mission is to create accessible, useful, high quality free and open-source software for everyone, with no ads, trackers, data collection or paywalls inside. We deeply believe open source can and has proven itself to be immensely beneficial to the world.