# Code Guidelines

These guidelines describe how to write code in Carroted repositories to provide the best experience for Carroted members, as well as anyone else reading the code.

This applies to both Carroted members and external contributors.

-----

### 1. Follow the format already used in the project

When you join a project, read the code in it. Notice format such as bracket placement and indenting.
For example:

```js
// If you see the following in the project's code:
if (condition) {
    code();
}

// Don't write functions like this:
function()
{
  code();
}

// Write them like the format you saw:
function () {
    code();
}

// This helps provide a consistent experience.
```

-----

### 2. Add comments to unclear code

When you have incredibly basic code, such as a basic addition or defining a string, comments are not needed. However, most code should be commented.
For example:

```js
var name = "Joseph";
name += " is my name.";

// No comments needed above

function linearCompute()
{
   var x = 2014 % 20;
   var y = 20 + x / 103;
   y = x ^ y;
}

// Comments needed above
```

-----

### 3. Use clear naming

Make sure you use clear names in your function and variable names.
For example:

```js
// clear names
var carrotedFoundedYear = 2021;
function getCarrotedFoundedYear() {
    return carrotedFoundedYear;
}

// unclear names
var year = 2021;
function getY() {
    return year;
}
```

-----

## That's it!

That's all there is! Thank you for following these guidelines to create easy to read and consistent code.